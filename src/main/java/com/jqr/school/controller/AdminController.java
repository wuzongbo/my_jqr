package com.jqr.school.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/admin")
public class AdminController {
    @RequestMapping("/index")
    public String index() {

        return "admin/index";
    }

    @RequestMapping("/tologin")
    public String tologin() {

        return "admin/login";
    }

    @PostMapping("/login")
    public String login(String username, String password, String verifycode) {


        return "admin/login";
    }

}
