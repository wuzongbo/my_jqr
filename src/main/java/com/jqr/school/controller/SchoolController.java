package com.jqr.school.controller;


import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jqr.school.po.School;
import com.jqr.school.service.SchoolService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author wuzongbo
 * @since 2022-01-04
 */
@Controller
@RequestMapping("/admin/school")
public class SchoolController {
	@Autowired
	private SchoolService schoolService;
	@GetMapping("/index")
	public String index(Model model) {
		
		QueryWrapper<School> queryWrapper=new QueryWrapper<>();
		queryWrapper.select("id","school_name","address","enable");
//		Page<School> page=new Page<>();
//
//		schoolService.page(page, queryWrapper);
//
//		model.addAttribute("page", page);
		List<School> schoolList=schoolService.list(queryWrapper);
		model.addAttribute("schoolList",schoolList);
		return "admin/school/index";
		
		
	}
	@GetMapping("/toedit")
	public String toedit(Model model,Integer id) {
		School school=null;
		if(id!=null){
			school=schoolService.getById(id);
			model.addAttribute("school",school);
		}
		else{
			school=new School();
		}
		model.addAttribute("school",school);
		return "admin/school/edit";
	}
	
	@PostMapping("/edit")
	public String edit(Model model, @Validated School school, BindingResult result) {
		if(result.hasErrors()){
			return "admin/role/edit";
		}
		if(school!=null&&school.getId()!=null){
			schoolService.updateById(school);
		}
		else {
			schoolService.save(school);
		}
		
		return "redirect:index";
	}
	@GetMapping("/delete")
	public String delete(Integer id) {
		if(id!=null){
			schoolService.removeById(id);
		}
		return "redirect:index";
	}

}
