package com.jqr.school.controller;


import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jqr.school.po.Department;
import com.jqr.school.service.DepartmentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <p>
 * ���� 前端控制器
 * </p>
 *
 * @author wuzongbo
 * @since 2022-01-04
 */
@Controller
@RequestMapping("/admin/depart")
public class DepartmentController {
	@Autowired
	private DepartmentService departmentService;
	@RequestMapping("/index")
	public String index(Model model) {
		QueryWrapper<Department> wrapper=new QueryWrapper<>();
		wrapper.select("id,depart_name","depart_code","school_id");
		
//		Page<Department> page=new Page<>();
//
//		departmentService.page(page, wrapper);
		
//		model.addAttribute("page", page);
		List<Department> departmentList=departmentService.list(wrapper);
		model.addAttribute("departmentList",departmentList);
		return "admin/depart/index";
	}
	@RequestMapping("/toedit")
	public String toedit(Model model ,Integer id) {
		Department department=null;
		if(id!=null){
			department=departmentService.getById(id);
		}
		else{
			department=new Department();
		}
		model.addAttribute("depart",department);
		return "admin/depart/edit";
	}
	@PostMapping("/edit")
	public String edit( @Validated Department department, BindingResult result) {
		if(result.hasErrors()){
			return "admin/depart/edit";
		}
		if(department!=null&&department.getId()!=null){
			departmentService.updateById(department);
		}
		else {
			departmentService.save(department);
		}
		return "redirect:index";
	}
	@RequestMapping("/delete")
	public String delete(@NotNull Integer id) {
		if(id!=null){
			departmentService.removeById(id);
		}
		return "redirect:index";
	}
	

}
