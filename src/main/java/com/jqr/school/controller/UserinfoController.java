package com.jqr.school.controller;


import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jqr.school.po.Userinfo;
import com.jqr.school.service.UserinfoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author wuzongbo
 * @since 2022-01-04
 */
@Controller
@RequestMapping("/admin/user")
public class UserinfoController {
	
	@Autowired
	private UserinfoService userinfoService;
	@GetMapping("/index")
	public String index(Model model) {
		QueryWrapper<Userinfo> queryWrapper=new QueryWrapper<>();
		queryWrapper.select("id","user_name","user_type");
		
		Page<Userinfo> page=new Page<>();
		
		userinfoService.page(page, queryWrapper);
		
		model.addAttribute("page", page);
		
		return "admin/user/index";

	}
	@GetMapping("/toedit")
	public String toedit(Model model, Integer id){
		Userinfo userinfo=null;
		if(id!=null){
			userinfo=userinfoService.getById(id);

		}
		else{
			userinfo=new Userinfo();
		}
		model.addAttribute("userinfo",userinfo);
		return  "admin/user/edit";

	}
	@PostMapping("/edit")
	public  String edit(@Validated Userinfo userinfo, BindingResult result){
		if(result.hasErrors()){
		   return 	"admin/user/edit";
		}
		if(userinfo.getId()!=null){

			userinfoService.updateById(userinfo);
		}
		else {
			userinfoService.save(userinfo);
		}
		return  "redirect:index";

	}
	public String delete(Integer id){
		if(id!=null){
			userinfoService.removeById(id);
		}
		return  "redirect:index";
	}

}
