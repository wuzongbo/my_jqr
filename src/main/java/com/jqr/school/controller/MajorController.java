package com.jqr.school.controller;


import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jqr.school.po.Major;
import com.jqr.school.service.MajorService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <p>
 * רҵ 前端控制器
 * </p>
 *
 * @author wuzongbo
 * @since 2022-01-04
 */
@Controller
@RequestMapping("/admin/major")
public class MajorController {
	@Autowired
	private MajorService majorService;
	@GetMapping("/index")
	public String index(Model model) {
		QueryWrapper<Major> wrapper=new QueryWrapper<>();
		wrapper.select("id","major_name","depart_id","create_year");
		
//		Page<Major> page=new Page<>();
//
//		majorService.page(page, wrapper);
//
//		model.addAttribute("page", page);
		List<Major> majorList=majorService.list(wrapper);
		model.addAttribute("majorList",majorList);
		return "admin/major/index";
		
	}
	@GetMapping("/toedit")
	public String toedit(Model model,Integer id) {
		Major major=null;
		if(id!=null){
			major=majorService.getById(id);
		}
		else{
			major=new Major();
		}
		model.addAttribute("major",major);
		
		return "admin/major/edit";
	}
	@PostMapping("/edit")
	public String edit( @Validated Major major, BindingResult result) {
		if(result.hasErrors()){
			return "admin/major/edit";
		}
		if(major!=null&&major.getId()!=null){
			majorService.updateById(major);
		}
		else {
			majorService.save(major);

		}
		return "redirect:index";
	}
	@GetMapping("/delete")
	public String delete(@NotNull Integer id) {
		if(id!=null){
			majorService.removeById(id);
		}
		return "redirect:index";
		
	}
}
