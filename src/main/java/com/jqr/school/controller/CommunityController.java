package com.jqr.school.controller;


import com.jqr.school.po.School;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jqr.school.po.Community;
import com.jqr.school.service.CommunityService;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <p>
 *   社团管理
 * </p>
 *
 * @author wuzongbo
 * @since 2022-01-01
 */
@Controller
@RequestMapping("/admin/community")
public class CommunityController {
	//注入
	@Autowired
	private CommunityService communityService;
	@GetMapping("/index")
	public String index(Model model) {
		QueryWrapper<Community> wrapper=new QueryWrapper<>();
		wrapper.select("id","name","type","teachers","depart","major","logo","status");
		
       // Page<Community> page=new Page<>(1,20);
	   // communityService.page(page,wrapper);
	    
	   // model.addAttribute("page", page);
	   /// model.addAttribute("title", "社团管理页面");
		List<Community> communityList=communityService.list(wrapper);
		 model.addAttribute("communityList", communityList);
		return "admin/community/index";
		
	}
	@GetMapping("/toedit")
	public String toedit(Model model, Integer id) {
		Community community=null;
		if(id!=null){
			community=communityService.getById(id);
		}
		else{
			community=new Community();
		}
		model.addAttribute("community",community);
		return "admin/community/edit";
	}
	@PostMapping("/edit")
	public String edit(Model model, @Validated Community community, BindingResult result) {
		if(result.hasErrors()){
			return  "admin/community/edit";
		}
		if(community!=null&&community.getId()!=null){
			communityService.updateById(community);
		}
		else
		{
			communityService.save(community);
		}
		
		return "redirect:index";
		
	}
	@GetMapping("/delete")
	public String delete(@NotNull Integer id) {
		if(id!=null){
			communityService.removeById(id);
		}
		return "redirect:index";
		
	}

}
