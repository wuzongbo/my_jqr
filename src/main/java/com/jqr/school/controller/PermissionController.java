package com.jqr.school.controller;


import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jqr.school.po.Permission;
import com.jqr.school.service.PermissionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author wuzongbo
 * @since 2022-01-04
 */
@Controller
@RequestMapping("/admin/permission")
public class PermissionController {
	
	@Autowired
	private PermissionService permissionService;
	@GetMapping("/index")
	public String index(Model model) {
		QueryWrapper<Permission> queryWrapper=new QueryWrapper<>();
		
		queryWrapper.select("id","code","description","url","type");
		
//		Page<Permission> page=new Page<>(1,20);
//
//		permissionService.page(page, queryWrapper);
//
//
//		model.addAttribute("page", page);
		List<Permission> permissionList=permissionService.list(queryWrapper);
		model.addAttribute("permissionList",permissionList);
		return "admin/permission/index";
		
		
	}
	@GetMapping("/toedit")
	public String toedit(Model model,Integer id) {
		Permission permission=null;
		if(id!=null){
		    permission=	permissionService.getById(id);
		   model.addAttribute("permission",permission);
		}
		else{
			permission=new Permission();
		}
		model.addAttribute("permission",permission);
		return "admin/permission/edit";
		
		
	}
	@PostMapping("/edit")
	public String edit(@Valid Permission permission, BindingResult bindingResult) {
		if(bindingResult.hasErrors()){
			 return "admin/permission/edit";
		}
		if(permission.getId()!=null){

			permissionService.updateById(permission);
		}
		else{
			permissionService.save(permission);

		}
		
		return "redirect:index";
		
		
	}
	@GetMapping("/delete")
	public String delete(@NotNull Integer id) {
		if(id!=null){
			permissionService.removeById(id);
		}

		return "redirect:index";
	}

}
