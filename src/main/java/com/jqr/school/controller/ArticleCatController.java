package com.jqr.school.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jqr.school.po.Article;
import com.jqr.school.po.ArticleCat;
import com.jqr.school.service.ArticleCatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author wuzongbo
 * @since 2022-01-18
 */
@Controller
@RequestMapping("/admin/articlecat")
public class ArticleCatController {

    @Autowired
    private ArticleCatService articleCatService;
    @RequestMapping("/index")
    public  String index(Model model){
        QueryWrapper<ArticleCat> queryWrapper=new QueryWrapper<>();
        queryWrapper.select("id","cat_name");
        List<ArticleCat> articleCatList=articleCatService.list(queryWrapper);
        model.addAttribute("articleCatList",articleCatList);
        return  "admin/articlecat/index";

    }
    @GetMapping("/toedit")
    public  String toeidt(Model model,Integer id){
        ArticleCat cat=null;
        if(id!=null){
            cat=articleCatService.getById(id);
            model.addAttribute("cat",cat);
        }
        else {
            cat=new ArticleCat();
        }
        model.addAttribute("cat",cat);
        return  "admin/articlecat/edit";
    }
    @PostMapping("/edit")
    public String edit(@Validated ArticleCat articleCat, BindingResult result){
        if(result.hasErrors()){
            return  "admin/articlecat/edit";
        }
        if(articleCat!=null&&articleCat.getId()!=null){
            articleCatService.updateById(articleCat);
        }
        else {
            articleCat=new ArticleCat();
        }
        return "redirect:index";
    }
    @GetMapping("delete")
    public  String delete(@NotNull Integer id){
        if(id!=null){
            articleCatService.removeById(id);
        }

        return "redirect:index";
    }
}
