package com.jqr.school.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author wuzongbo
 * @since 2022-03-08
 */
@Controller
@RequestMapping("/school/roleFrontendMenu")
public class RoleFrontendMenuController {

}
