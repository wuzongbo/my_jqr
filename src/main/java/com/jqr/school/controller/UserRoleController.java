package com.jqr.school.controller;

import com.jqr.school.po.Result;
import com.jqr.school.po.Role;
import com.jqr.school.po.RolePermission;
import com.jqr.school.po.UserRole;
import com.jqr.school.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author wuzongbo
 * @since 2022-01-04
 */
@Controller
@RequestMapping("/school/userRole")
public class UserRoleController {
    @Autowired
    private UserRoleService userRoleService;
    @PostMapping("/add")
    @ResponseBody
    public Result add(UserRole userRole){
        boolean isAdd= userRoleService.save(userRole);
        Result result=null;
        if(isAdd){
            result=new Result();
            result.setCode(1);
            result.setMsg("添加成功");
        }
        else {
            result.setCode(0);
            result.setMsg("添加失败");
        }
        return  result;

    }

}
