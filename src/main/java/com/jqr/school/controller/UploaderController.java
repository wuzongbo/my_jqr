package com.jqr.school.controller;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.UUID;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.jqr.school.po.CosUploadResult;
import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.model.PutObjectRequest;
import com.qcloud.cos.model.PutObjectResult;
import com.qcloud.cos.region.Region;

@Controller
@RequestMapping("/admin/upload")
public class UploaderController {

	@Value("${spring.txcos.secretId}")
	private String secretId;
	@Value("${spring.txcos.secretKey}")
	private String secretKey;
	@Value("${spring.txcos.bucket}")
	private String bucket;
	@Value("${spring.txcos.bucketName}")
	private String bucketName;
	@Value("${spring.txcos.path}")
	private String path;
	@Value("${spring.txcos.prefix}")
	private String prefix;
	@PostMapping("/cosupload")
	@ResponseBody
	public CosUploadResult cosUpload(@RequestParam(value = "file") MultipartFile file, HttpSession session) {

		if (file == null) {
			return new CosUploadResult(0,  null,"文件为空");
		}
		String oldFileName = file.getOriginalFilename();
		String eName = oldFileName.substring(oldFileName.lastIndexOf("."));
		String newFileName = UUID.randomUUID() + eName;
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		int day = cal.get(Calendar.DATE);
		// 1 初始化用户身份信息(secretId, secretKey)
		COSCredentials cred = new BasicCOSCredentials(secretId, secretKey);
		// 2 设置bucket的区域, COS地域的简称请参照
		// https://cloud.tencent.com/document/product/436/6224
		ClientConfig clientConfig = new ClientConfig(new Region(bucket));
		// 3 生成cos客户端
		COSClient cosclient = new COSClient(cred, clientConfig);
		// bucket的命名规则为{name}-{appid} ，此处填写的存储桶名称必须为此格式
		String bucketName = this.bucketName;
		// 简单文件上传, 最大支持 5 GB, 适用于小文件上传, 建议 20 M 以下的文件使用该接口
		// 大文件上传请参照 API 文档高级 API 上传
		File localFile = null;
		try {
			localFile = File.createTempFile("temp", null);
			file.transferTo(localFile);
			// 指定要上传到 COS 上的路径
			String key = "/" + this.prefix + "/" + year + "/" + month + "/" + day + "/" + newFileName;
			PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, key, localFile);
			PutObjectResult putObjectResult = cosclient.putObject(putObjectRequest);
			return new CosUploadResult(1,  this.path + putObjectRequest.getKey(),"上传成功");
		} catch (IOException e) {
			return new CosUploadResult(-1,  null,e.getMessage());
		} finally {
			// 关闭客户端(关闭后台线程)
			cosclient.shutdown();
		}

	}

}
