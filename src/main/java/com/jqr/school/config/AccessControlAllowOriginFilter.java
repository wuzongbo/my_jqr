package com.jqr.school.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
/**
 * 解决跨域问题
 */
@Configuration
public class AccessControlAllowOriginFilter extends WebMvcConfigurerAdapter {
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                //跨域配置报错，将.allowedOrigins替换成.allowedOriginPatterns即可。
                //问题：When allowCredentials is true, allowedOrigins cannot contain the special value "*" since that cannot
                //.allowedOrigins("*")


                .allowedOriginPatterns("*")
                // 是否允许证书（cookies）
               // .allowCredentials(true)
                .allowedMethods("PUT", "DELETE", "GET", "POST", "OPTIONS")
                .allowedHeaders("*")
                .exposedHeaders("access-control-allow-headers",
                        "access-control-allow-methods",
                        "access-control-allow-origin",
                        "access-control-max-age",
                        "X-Frame-Options")
                .allowCredentials(true).maxAge(3600);
    }
}
