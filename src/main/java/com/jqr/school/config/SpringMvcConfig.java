package com.jqr.school.config;

import java.util.Locale;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//扩展SpringMVC，官方建议
@Configuration
public class SpringMvcConfig  implements WebMvcConfigurer{

	
	//视图跳转
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		// TODO Auto-generated method stub
		
		//设置默认首页
	    registry.addViewController("/").setViewName("static/index");
	    registry.addViewController("/index.html").setViewName("static/index");

	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {

		registry.addResourceHandler("/static/**")
				.addResourceLocations("classpath:/static/");
	}

	//模板引擎（视图）:thymaleaf,volitity,jsp,freemaker
	
//	public ViewResolver myViewResolver() {
//		
//		return  new MyViewResolver();
//	}
//	
//	public static class MyViewResolver implements ViewResolver{
//		
//		
//		@Override
//		public View resolveViewName(String viewName, Locale locale) throws Exception {
//			// TODO Auto-generated method stub
//			return null;
//		}
//		
//		
//	}
	
	

}
