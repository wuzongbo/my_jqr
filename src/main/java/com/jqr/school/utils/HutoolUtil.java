package com.jqr.school.utils;

import cn.hutool.crypto.digest.DigestUtil;
import org.springframework.stereotype.Component;

@Component
public class HutoolUtil {

    public  String MD5(String psw){
        //Digester md5=new Digester(DigestAlgorithm.MD5);
       // String password=md5.digestHex(psw);
        String password= DigestUtil.md5Hex(psw);
        return password;
    }
}
