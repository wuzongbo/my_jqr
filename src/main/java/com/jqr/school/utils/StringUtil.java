package com.jqr.school.utils;

import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import java.util.List;

public class StringUtil {

    public static  String validatedResult(BindingResult result){
        String msg=null;
        List<ObjectError> errorList= result.getAllErrors();
        for (int i=0;i<errorList.size();i++){
            msg+=errorList.get(i).getDefaultMessage()+"<br/>";
        }
        return  msg;
    }
}
