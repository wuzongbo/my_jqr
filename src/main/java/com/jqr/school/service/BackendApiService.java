package com.jqr.school.service;

import com.jqr.school.po.BackendApi;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wuzongbo
 * @since 2022-03-08
 */
public interface BackendApiService extends IService<BackendApi> {
    List<BackendApi> getApiUrlByUserName(String username);

}
