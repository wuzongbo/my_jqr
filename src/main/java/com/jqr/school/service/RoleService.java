package com.jqr.school.service;

import com.jqr.school.po.Role;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wuzongbo
 * @since 2022-01-04
 */
public interface RoleService extends IService<Role> {

}
