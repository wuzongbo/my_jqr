package com.jqr.school.service;

import com.jqr.school.po.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wuzongbo
 * @since 2022-01-04
 */
public interface UserRoleService extends IService<UserRole> {
    List<String> getRolesByUserId(Integer id);


}
