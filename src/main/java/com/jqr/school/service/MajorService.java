package com.jqr.school.service;

import com.jqr.school.po.Major;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * רҵ 服务类
 * </p>
 *
 * @author wuzongbo
 * @since 2022-01-04
 */
public interface MajorService extends IService<Major> {

}
