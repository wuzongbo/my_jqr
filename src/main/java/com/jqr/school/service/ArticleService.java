package com.jqr.school.service;

import com.jqr.school.po.Article;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wuzongbo
 * @since 2022-01-18
 */
public interface ArticleService extends IService<Article> {

}
