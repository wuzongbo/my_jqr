package com.jqr.school.service;

import com.jqr.school.po.ArticleCat;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wuzongbo
 * @since 2022-01-18
 */
public interface ArticleCatService extends IService<ArticleCat> {

}
