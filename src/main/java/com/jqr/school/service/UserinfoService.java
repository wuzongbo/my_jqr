package com.jqr.school.service;

import com.jqr.school.po.Userinfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wuzongbo
 * @since 2022-01-04
 */
public interface UserinfoService extends IService<Userinfo>  {
    boolean login(String username,String password);
    boolean register(Userinfo userinfo);
    Userinfo getUserByUserName(String username);
    boolean checkLogin(String username,String password) throws Exception;

}
