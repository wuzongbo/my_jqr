package com.jqr.school.service.impl;

import com.jqr.school.po.Community;
import com.jqr.school.mapper.CommunityMapper;
import com.jqr.school.service.CommunityService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * ���� 服务实现类
 * </p>
 *
 * @author wuzongbo
 * @since 2022-01-01
 */
@Service
public class CommunityServiceImpl extends ServiceImpl<CommunityMapper, Community> implements CommunityService {

}
