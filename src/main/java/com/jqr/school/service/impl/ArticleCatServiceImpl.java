package com.jqr.school.service.impl;

import com.jqr.school.po.ArticleCat;
import com.jqr.school.mapper.ArticleCatMapper;
import com.jqr.school.service.ArticleCatService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuzongbo
 * @since 2022-01-18
 */
@Service
public class ArticleCatServiceImpl extends ServiceImpl<ArticleCatMapper, ArticleCat> implements ArticleCatService {

}
