package com.jqr.school.service.impl;

import com.jqr.school.po.RoleFrontendMenu;
import com.jqr.school.mapper.RoleFrontendMenuMapper;
import com.jqr.school.service.RoleFrontendMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuzongbo
 * @since 2022-03-08
 */
@Service
public class RoleFrontendMenuServiceImpl extends ServiceImpl<RoleFrontendMenuMapper, RoleFrontendMenu> implements RoleFrontendMenuService {

}
