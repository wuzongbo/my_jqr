package com.jqr.school.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jqr.school.mapper.RoleMapper;
import com.jqr.school.po.Role;
import com.jqr.school.po.UserRole;
import com.jqr.school.mapper.UserRoleMapper;
import com.jqr.school.service.UserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuzongbo
 * @since 2022-01-04
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

    @Autowired
    private UserRoleMapper userRoleMapper;
    @Autowired
    private RoleMapper roleMapper;
    @Override
    public List<String> getRolesByUserId(Integer id) {
        UserRole userRole=userRoleMapper.selectById(id);
        String roleIds=userRole.getRoleIds();
        String[] roleIdsArr=roleIds.split(",");

        return getRoleNames(roleIdsArr);
    }
    private List<String> getRoleNames(String ...roleIdsArr){

        QueryWrapper<Role> queryWrapper=new QueryWrapper<>();
        queryWrapper.in("id",roleIdsArr);

        List<Role> roleList=roleMapper.selectList(queryWrapper);
        List<String> roleNames=null;
        for (Role role : roleList) {
            roleNames  = new ArrayList<>();
            roleNames.add(role.getRoleName());
        }

        return roleNames;
    }
}
