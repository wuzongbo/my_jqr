package com.jqr.school.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.jqr.school.po.Userinfo;
import com.jqr.school.mapper.UserinfoMapper;
import com.jqr.school.service.UserinfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuzongbo
 * @since 2022-01-04
 */
@Service
public class UserinfoServiceImpl extends ServiceImpl<UserinfoMapper, Userinfo> implements UserinfoService {

    @Autowired
    private  UserinfoMapper userinfoMapper;
    @Override
    public boolean login(String username, String password) {

        return false;
    }

    @Override
    public boolean register(Userinfo userinfo) {
        return false;
    }

    @Override
    public Userinfo getUserByUserName(String username) {

        QueryWrapper<Userinfo> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("user_name",username);
        Userinfo userinfo=userinfoMapper.selectOne(queryWrapper);
        return userinfo;
    }

    @Override
    public boolean checkLogin(String username, String password)  throws Exception{
        
    		return true;
    }
}
