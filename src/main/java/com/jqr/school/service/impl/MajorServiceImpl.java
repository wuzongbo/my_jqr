package com.jqr.school.service.impl;

import com.jqr.school.po.Major;
import com.jqr.school.mapper.MajorMapper;
import com.jqr.school.service.MajorService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * רҵ 服务实现类
 * </p>
 *
 * @author wuzongbo
 * @since 2022-01-04
 */
@Service
public class MajorServiceImpl extends ServiceImpl<MajorMapper, Major> implements MajorService {

}
