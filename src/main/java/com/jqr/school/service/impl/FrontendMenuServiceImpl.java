package com.jqr.school.service.impl;

import com.jqr.school.po.FrontendMenu;
import com.jqr.school.mapper.FrontendMenuMapper;
import com.jqr.school.service.FrontendMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuzongbo
 * @since 2022-03-08
 */
@Service
public class FrontendMenuServiceImpl extends ServiceImpl<FrontendMenuMapper, FrontendMenu> implements FrontendMenuService {

}
