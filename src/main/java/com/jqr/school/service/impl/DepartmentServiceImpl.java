package com.jqr.school.service.impl;

import com.jqr.school.po.Department;
import com.jqr.school.mapper.DepartmentMapper;
import com.jqr.school.service.DepartmentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * ���� 服务实现类
 * </p>
 *
 * @author wuzongbo
 * @since 2022-01-04
 */
@Service
public class DepartmentServiceImpl extends ServiceImpl<DepartmentMapper, Department> implements DepartmentService {

}
