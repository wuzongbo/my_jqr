package com.jqr.school.service.impl;

import com.jqr.school.po.RoleBackendApi;
import com.jqr.school.mapper.RoleBackendApiMapper;
import com.jqr.school.service.RoleBackendApiService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuzongbo
 * @since 2022-03-08
 */
@Service
public class RoleBackendApiServiceImpl extends ServiceImpl<RoleBackendApiMapper, RoleBackendApi> implements RoleBackendApiService {

}
