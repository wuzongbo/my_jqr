package com.jqr.school.service.impl;

import com.jqr.school.po.Permission;
import com.jqr.school.mapper.PermissionMapper;
import com.jqr.school.service.PermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuzongbo
 * @since 2022-01-04
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements PermissionService {

}
