package com.jqr.school.service.impl;

import com.jqr.school.po.BackendApi;
import com.jqr.school.mapper.BackendApiMapper;
import com.jqr.school.service.BackendApiService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuzongbo
 * @since 2022-03-08
 */
@Service
public class BackendApiServiceImpl extends ServiceImpl<BackendApiMapper, BackendApi> implements BackendApiService {

    @Override
    public List<BackendApi> getApiUrlByUserName(String username) {
        return null;
    }
}
