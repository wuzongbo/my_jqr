package com.jqr.school.service;

import com.jqr.school.po.RoleFrontendMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wuzongbo
 * @since 2022-03-08
 */
public interface RoleFrontendMenuService extends IService<RoleFrontendMenu> {

}
