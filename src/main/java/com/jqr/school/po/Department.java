package com.jqr.school.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * ����
 * </p>
 *
 * @author wuzongbo
 * @since 2022-01-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="Department对象", description="����")
public class Department implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "部门名称")
    private String departName;

    @ApiModelProperty(value = "校区")
    private String campus;

    @ApiModelProperty(value = "部门英文名称")
    private String departCode;

    @ApiModelProperty(value = "部门描述")
    private String departDesc;

    @ApiModelProperty(value = "学校id")
    private Integer schoolId;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    @TableLogic
    private Integer deleted;


}
