package com.jqr.school.po;

public enum ResultEnum {
    SUCCESS("成功",1)
    ,ERROR("失败",0)
    ,NO_DATA("没有数据",2)
    ,NOT_FOUND("未找到资源",404)
    ,INTERNAL_SERVER_ERROR("服务器内部错误",500);
    //成员变量
    private String name;
    private int index;

    //构造方法
    private ResultEnum(String name,int index)
    {
        this.name=name;
        this.index=index;
    }
    //覆盖方法
    @Override
    public String toString()
    {
        return this.index+"-"+this.name;
    }
}
