package com.jqr.school.po;

import com.baomidou.mybatisplus.annotation.Version;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuzongbo
 * @since 2022-03-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="BackendApi对象", description="")
public class BackendApi implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private String backendApiId;

    @ApiModelProperty(value = "API名称")
    private String backendApiName;

    @ApiModelProperty(value = "API请求地址")
    private String backendApiUrl;

    @ApiModelProperty(value = "API请求方式：GET、POST、PUT、DELETE")
    private String backendApiMethod;

    @ApiModelProperty(value = "父ID")
    private String pid;

    @ApiModelProperty(value = "排序")
    private Integer backendApiSort;

    @ApiModelProperty(value = "描述")
    private String description;


}
