package com.jqr.school.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CosUploadResult {
	private Integer status;
	private String path;
	private String msg;

}
