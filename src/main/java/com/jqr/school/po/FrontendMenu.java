package com.jqr.school.po;

import com.baomidou.mybatisplus.annotation.Version;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuzongbo
 * @since 2022-03-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="FrontendMenu对象", description="")
public class FrontendMenu implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private String frontendMenuId;

    @ApiModelProperty(value = "前端菜单名称")
    private String frontendMenuName;

    @ApiModelProperty(value = "前端菜单访问HTML地址")
    private String frontendMenuUrl;

    @ApiModelProperty(value = "父ID")
    private String pid;

    @ApiModelProperty(value = "排序")
    private Integer frontendMenuSort;

    @ApiModelProperty(value = "描述")
    private String description;


}
