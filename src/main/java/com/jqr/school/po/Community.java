package com.jqr.school.po;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.Version;

import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * ����
 * </p>
 *
 * @author wuzongbo
 * @since 2022-01-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="Community对象", description="社团")
public class Community implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String name;

    @ApiModelProperty(value = "ͨ0 通用社团 1专业社团")
    private Integer type;

    @ApiModelProperty(value = "导师，社团必须有一个指导老师，教师编号如：1,13")
    private String teachers;

  

    @ApiModelProperty(value = "学院编号")
    private Integer depart;

    @ApiModelProperty(value = "专业,处室创建的社团，没有专业")
    private Integer major;

    @ApiModelProperty(value = "团社logo")
    private String logo;

    @ApiModelProperty(value = "团社会长")
    private Integer charger;

    @ApiModelProperty(value = "团社介绍")
    private String description;

    @ApiModelProperty(value = "0 审核，1激活，2运营中，3终止")
    private Integer status;

    @ApiModelProperty(value = "团社地址")
    private String address;

    @ApiModelProperty(value = "团社电话")
    private String mobile;

    @ApiModelProperty(value = "团社邮箱")
    private String email;

    @ApiModelProperty(value = "团社公众号")
    private String weixin;

    @ApiModelProperty(value = "学校编号")
    private Integer schoolId;//school_id

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
    @ApiModelProperty(value = "乐观锁")
    @Version
    private Integer version;
    @ApiModelProperty(value = "逻辑删除")
    @TableLogic
    private Integer deleted;


}
