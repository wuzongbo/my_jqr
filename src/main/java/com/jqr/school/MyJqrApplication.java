package com.jqr.school;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.jqr.school.mapper")
public class MyJqrApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyJqrApplication.class, args);
	}

}
