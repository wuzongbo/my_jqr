package com.jqr.school.mapper;

import com.jqr.school.po.FrontendMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wuzongbo
 * @since 2022-03-08
 */
@Repository
public interface FrontendMenuMapper extends BaseMapper<FrontendMenu> {

}
