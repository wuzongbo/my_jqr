package com.jqr.school.mapper;

import com.jqr.school.po.RoleFrontendMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wuzongbo
 * @since 2022-03-08
 */
@Repository
public interface RoleFrontendMenuMapper extends BaseMapper<RoleFrontendMenu> {

}
