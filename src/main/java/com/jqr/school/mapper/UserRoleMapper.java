package com.jqr.school.mapper;

import com.jqr.school.po.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wuzongbo
 * @since 2022-01-04
 */
@Repository
public interface UserRoleMapper extends BaseMapper<UserRole> {

}
