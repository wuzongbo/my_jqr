package com.jqr.school.handler;

import java.time.LocalDateTime;
import java.util.Date;

import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
@Component
public class DateMetaObjectHandler implements MetaObjectHandler {

	@Override
	public void insertFill(MetaObject metaObject) {
		// TODO Auto-generated method stub
		this.strictInsertFill(metaObject,"createTime", Date.class, new Date());
		this.strictUpdateFill(metaObject, "updateTime",  Date.class, new Date());
	}

	@Override
	public void updateFill(MetaObject metaObject) {
		// TODO Auto-generated method stub
		this.strictUpdateFill(metaObject, "updateTime",   Date.class, new Date());
	}

}
