package com.jqr.school;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jqr.school.mapper.RoleMapper;
import com.jqr.school.po.Role;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.security.crypto.bcrypt.BCrypt;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jqr.school.mapper.CommunityMapper;
import com.jqr.school.po.Community;

@SpringBootTest
class MyJqrApplicationTests {
	@Autowired
	private CommunityMapper communityMapper;

	@Test
	void contextLoads() {
		
	   // LambdaQueryChainWrapper<Community> wrapper=	new LambdaQueryChainWrapper<>(communityMapper);
//		QueryChainWrapper<Community> wrapper=new QueryChainWrapper<>(communityMapper);
//		QueryWrapper<Community>  wraper=new QueryWrapper<>();
		
		
		List<Community> communitys=communityMapper.selectList(null);
		
		System.out.println(communitys);
		
	}
	@Test
	public void testSelectByBatchId() {
		List<Integer> ids=new ArrayList<>();
		ids.add(32);
		
		ids.add(33);
		ids.add(34);
		List<Community> communities=communityMapper.selectBatchIds(ids);
		
		System.out.println(communities);
		
	}
	@Test
	public void testSelectByMap() {
		
		Map<String,Object> map=new HashMap<>();
		
		map.put("type",0);
		map.put("name", "乒乓球社");
		
		List<Community> communities=communityMapper.selectByMap(map);
		System.out.println(communities);
	}
	
	@Test
	public void testSelectByPage() {
		//1.分页插件
		//创建Page实例  
		//参数1 当前页
		//参数2 页面大小
	
		Page<Community> page=new Page<>(1,5);
		
		communityMapper.selectPage(page, null);
		page.getRecords().forEach(System.out::println);
		
	}
	
	@Test
	public void testupdate() {
		communityMapper.deleteById(32);
	}
	@Test
	public void testBCrypt() {
//	    String  psw=	BCrypt.hashpw("123456", BCrypt.gensalt());
//	    
//	    //$2a$10$OWCULblqMoFaXr6BgTsgquGo12q.Q2FQIdUgrX4vpWLFyOXMwQgYy
//		boolean ok=BCrypt.checkpw("123456", "$2a$10$OWCULblqMoFaXr6BgTsgquGo12q.Q2FQIdUgrX4vpWLFyOXMwQgYy");
//		System.out.println(ok);
	}
	@Autowired
	private RoleMapper roleMapper;
	@Test
	public void testAddRole() {
		    Role role=new Role();
			role.setRoleName("测试角色3");
			role.setStatus(1);
			role.setDescription("测试角色3");
			roleMapper.insert(role);
	}
	
	
}
